﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class maintains all player data that the client receives from the server when in game mode. This is so that the client program can display
 *                      all players and their scores dyanamically.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : IEquatable<Player>, IComparable<Player> {

    public int score;
    public string name;

    public Player(string name, int score)
    {
        this.score = score;
        this.name = name;
    }

    public int CompareTo(Player comparePlayer)
    {
        if (comparePlayer == null)
            return 1;

        return this.score.CompareTo(comparePlayer.score);
    }

    public bool Equals(Player other)
    {
        if (other == null)
            return false;

        if (this.score == other.score) return true;
        return false;
    }
}
