﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This Client class is the networking core on the client side. It provides the connection from user to server. It also provides methods for other classes to get 
 *                      specific data from the server through sending requests. It also maintains all of the responses from the server executes each of them, one by one. Not only is it
 *                      the networking core of the client program, but it also maintains all of the user's game data, including name, score, entered room, login status, and playing status.
 *                      This class is also maintains game data as well, such as a list of all players game timer, and letters to press.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class Client : MonoBehaviour
{ 
    public volatile string name = "";
    private volatile static Socket clientSocket;
    private IPAddress ipAddress;
    private int port = 8000;
    private static byte[] dataBuffer = new byte[1024];
    public volatile bool connectionStarted = false;
    public volatile bool login = false;
    public volatile bool playing = false;
    public volatile bool ready = false;
    public volatile int room;
    public Text text;
    
    private volatile List<string> responseBuffer;

    public GameObject roomsScreen;
    public volatile bool enteredRoom = false;

    /*-----Game data----------*/
    public volatile int score = 0;
    public List<Player> playerList;
    public volatile int countDown = 4;
    public volatile int gameCountDown = 60;
    public volatile char letter1 = '0';
    public volatile char letter2 = '0';
    public volatile char letter3 = '0';
    /*------------------------*/

    public bool startConnection()
    {
        clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        
        try
        {
            clientSocket.Connect(ipAddress, port);
            
            Thread.Sleep(500);
            if (!clientSocket.Connected) return false;
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            return false;
        }
        responseBuffer = new List<string>();
        connectionStarted = true;
        return true;
    }

    public bool setIP(string address)
    {
        try
        {
            ipAddress = IPAddress.Parse(address);
        }
        catch
        {
            return false;
        }
        if (ipAddress == null) return false;

        return true;
    }

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        Application.runInBackground = true;

    }

    void Update()
    {
        if (connectionStarted && !playing) //&& not playing
        {
            if (responseBuffer == null)
                responseBuffer = new List<string>();

            string recievemessage = "";
            if (clientSocket != null)
            {
                clientSocket.ReceiveTimeout = 50;
                int datareceived = 0;
                try
                {
                    datareceived = clientSocket.Receive(dataBuffer, 0, dataBuffer.Length, SocketFlags.None);
                }
                catch (SocketException e)
                {
                    return;
                }
                recievemessage = Encoding.ASCII.GetString(dataBuffer, 0, datareceived);

                if (recievemessage == "" || (recievemessage.Length > 4 && recievemessage.Substring(0 , 4) == "GAME"))
                {
                    return;
                }
                

                string[] contents = recievemessage.Split(';');
                if (contents != null) {
                    for (int i = 0; i < contents.Length; i++)
                    {
                        if (contents[i] != "")
                        {
                            responseBuffer.Add(contents[i]);
                            
                        }
                    }
                }

                
            }
        }
        else if (connectionStarted && playing)
        {
            
            string receiveMessage = "";
            clientSocket.ReceiveTimeout = 10;
            int datareceived = 0;
            try
            {
                datareceived = clientSocket.Receive(dataBuffer, 0, dataBuffer.Length, SocketFlags.None);
            }
            catch (SocketException e)
            {
                return;
            }
            receiveMessage = Encoding.ASCII.GetString(dataBuffer, 0, datareceived);


            if (receiveMessage.Length > 0)
            {
                string[] requests = receiveMessage.Split(';');
                for (int x = 0; x < requests.Length; x++)
                {
                    if (requests[x].Length >= 4 && requests[x].Substring(0, 4) == "GAME")
                    {

                        Debug.Log(requests[x]);
                        string[] contents = requests[x].Substring(5).Split();


                        int i = 0;
                        if (playerList == null)
                            playerList = new List<Player>();
                        else
                            playerList.Clear();

                        while (contents[i] != "|")
                        {
                            string tempName = contents[i];
                            int tempScore = 0;
                            while (true)
                            {

                                try
                                {
                                    tempScore = int.Parse(contents[i + 1]); //If there's an exception when parsing, it's probably part of the name, so just add it to the name
                                    break;
                                }
                                catch (Exception e)
                                {
                                    tempName += " " + contents[i + 1];
                                    i++;
                                }
                            }
                            Player temp = new Player(tempName, tempScore);
                            playerList.Add(temp);
                            i += 2;
                        }
                        sortPlayers();
                        i++;

                        countDown = int.Parse(contents[i++]);
                        gameCountDown = int.Parse(contents[i++]);
                        letter1 = contents[i++].ElementAt<char>(0);
                        letter2 = contents[i++].ElementAt<char>(0);
                        letter3 = contents[i++].ElementAt<char>(0);

                    }
                    string sendMessage = "Point " + score + ";";
                    clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
                }

            }

        }
            
        
    }

    private void OnApplicationQuit()
    {
        if (enteredRoom)
            exitRoom();
        if (login)
            Disconnect();
    }


    public void Disconnect()
    {
        
        //playing = false;
      string sendmessage = "Exit;";
        clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendmessage), 0, sendmessage.Length, SocketFlags.None);
        Thread.Sleep(100);
       A:
        Update();
        for (int i = 0; i < responseBuffer.Count; i++)
        {
            if (responseBuffer[i] == "Bye")
            {
                login = false;
                responseBuffer.Clear();
                playing = false;
                connectionStarted = false;
                clientSocket.Close();
                Thread.Sleep(100);
                return;
            }
        }
        
        goto A;
    }
      

    public int checkName(string s)
    {
        string sendmessage = "Check Name " + s + ";";
        clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendmessage), 0, sendmessage.Length, SocketFlags.None);
        Thread.Sleep(1000);
        connectionStarted = true;
        playing = false;

        for (int l = 0; l < 10; l++)
        {
            Update();
            for (int i = 0; i < responseBuffer.Count; i++)
            {
                if (responseBuffer[i] == "Doesn't exist")
                {
                    responseBuffer.RemoveAt(i);
                    name = s;
                    login = true;
                    return 0;
                }
                else if (responseBuffer[i] == "Exists")
                {
                    responseBuffer.RemoveAt(i);
                    Disconnect();
                    return -2;
                }
            }
        }

        return -4;

    }

    public string[] getAllRoomData()
    {
        string sendMessage = "";
        string[] retVal;

        sendMessage = "Get All Room Data;";

        clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
        Update();
        for (int i = 0; i < responseBuffer.Count; i++)
        {
            if (responseBuffer[i].Length >= 11 && responseBuffer[i].Substring(0, 11) == "AllRoomData")
            {
                retVal = responseBuffer[i].Substring(12).Split();
                responseBuffer.RemoveAt(i);
                return retVal;
            }
        }

        return null;
        
    }

    public bool enterRoom(int roomNumber)
    {
        string sendMessage = "";
        
            connectionStarted = true;
            playing = false;
            sendMessage = "Enter Room " + roomNumber + ";"; //Note: Rooms in client are up by 1 compared to rooms on server

            clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None); //Client sends a request to enter a room
            Thread.Sleep(500);

        while (true)
        {
            Update();
            
            for (int i = 0; i < responseBuffer.Count; i++)
            {
                if (responseBuffer[i] == "Cannot Enter Room")
                {
                    responseBuffer.RemoveAt(i);
                    return false;
                }
                else if (responseBuffer[i] == "Entered Room")
                {
                    responseBuffer.RemoveAt(i);
                    enteredRoom = true;
                    return true;
                }
            }

            
        }
       

    }

    public string[] getPlayerList()
    {
        string sendMessage = "";

        sendMessage = "Get Player List;";

        clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
        Update();
        for (int i = 0; i < responseBuffer.Count; i++)
        {
           if (responseBuffer[i].Length >= 7 && responseBuffer[i].Substring(0, 7) == "Players")
            {
                string[] retVal = responseBuffer[i].Substring(8).Split('|');
                responseBuffer.RemoveAt(i);
                return retVal;
            }
        }

        return null;

    }

    public bool exitRoom() 
    {
        resetClientData();
        string sendMessage = "";
        playing = false;
        while (true) {
            sendMessage = "Exit Room;";

            clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
            Thread.Sleep(500);
            Update();
            for (int i = 0; i < responseBuffer.Count; i++)
            {
                if (responseBuffer[i] == "Exited")
                {
                    responseBuffer.RemoveAt(i);
                    enteredRoom = false;
                    return true;
                }
            }

            
        }
    }

    public bool setReady()
    {
        playing = false;
        string sendMessage = "";
        sendMessage = "Ready;";

        clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
        Thread.Sleep(100);
        while (true)
        {
            Update();

            for (int i = 0; i < responseBuffer.Count; i++)
            {
                if (responseBuffer[i] == "Set To Ready")
                {
                    ready = true;
                    responseBuffer.RemoveAt(i);
                    return true;
                }
            }
        }

    }

    public bool checkIfReady()
    {
        for (int k = 0; k < 5; k++)
        {
            Update();
            for (int i = 0; i < responseBuffer.Count; i++)
            {

                if (responseBuffer[i] == "All good")
                {
                    responseBuffer.RemoveAt(i);
                    return true;
                }
            }
        }
        return false;

    }

    public void cancelReady()
    {
        string sendMessage = "";

        while (true)
        {
            sendMessage = "Cancel;";

            clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
            Update();

            for (int i = 0; i < responseBuffer.Count; i++)
            {
                if (responseBuffer[i] == "Cancelled")
                {
                    ready = false;
                    responseBuffer.RemoveAt(i);
                    return;
                }
            }

        }
    }
        

    public void playGame() //Only call this when all users are ready to play
    {
        string sendMessage = "";
        
        sendMessage = "Play;";
        clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
        Thread.Sleep(500);
        while (true)
        {
            Update();
            for (int i = 0; i < responseBuffer.Count; i++)
            {
                if (responseBuffer[i] == "Now Playing")
                {
                    responseBuffer.Clear();
                    resetClientData();
                    ready = false; //This will stop the DisplayPlayers class from checking if all players are ready
                    playing = true;
                    connectionStarted = true;
                    
                    return;
                }
            }
        }

        


    }

    private void resetClientData()
    {
        List<Player> playerList = new List<Player>();
        score = 0;
        countDown = 4;
        gameCountDown = 60;
        letter1 = '0';
        letter2 = '0';
        letter3 = '0';

    }

    public string getChat()
    {
        string sendMessage = "Get Chat;";
        clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
        for (int k = 0; k < 1; k++)
        {
            Update();
            for (int i = 0; i < responseBuffer.Count; i++)
            {
                if (responseBuffer[i].Length > 4 && responseBuffer[i].Substring(0, 4) == "Chat")
                {
                    string retVal = responseBuffer[i].Substring(5);
                    responseBuffer.RemoveAt(i);
                    return retVal;
                }
            }
        }

        return "";

    }

    public void sendChatMessage(string message)
    {
        string sendMessage = "Chat " + message + ";";
        clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
    }

    public void getFinalScores()
    {
        responseBuffer = null;
        responseBuffer = new List<string>();
        
        
        string sendMessage = "Get Final Scores;";
        clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
        for (int u = 0; u < 3; u++) 
        {
            playing = false;
            Update();

            for (int i = 0; i < responseBuffer.Count; i++)
            {
                if (responseBuffer[i].Length > 5 && responseBuffer[i].Substring(0, 5) == "Final")
                {
                    string[] contents = responseBuffer[i].Substring(6).Split('|');

                    for (int j = 0, k = 0; j < contents.Length; k++, j+=2)
                    {
                        if (contents[j] != "")
                        {
                            playerList[k].name = contents[j];
                            playerList[k].score = Int32.Parse(contents[j + 1]);
                        }
                    }
                    responseBuffer.RemoveAt(i);
                    break;
                }
            }
        }
        

    }

    public void sortPlayers()
    {
        playerList.Sort(delegate (Player a, Player b)
        {
            if (a == null && b == null) return 0;
            else if (a == null) return -1;
            else if (b == null) return 1;
            else return b.score.CompareTo(a.score);
        });
    }

    public void die()
    {
        Destroy(gameObject);
    }
}
