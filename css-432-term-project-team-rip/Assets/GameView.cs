﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: The GameView class is what manages the display in game mode. It takes all of the game data that the client receives and constantly displays that data on screen
 *                      (i.e. timer, scores, letters). It is also what executes sounds, what starts the game on the client side, and what ends the game on the client side.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class GameView : MonoBehaviour {
    public int countDown;
    public int gameCountDown = 60;
    public Text textDisplay;
    public Text countDownText;
    public Text score;
    public Text gameTime;
    public Canvas endScreen;
    public AudioSource[] music;
    public GameObject controller;
    private char prevLetter;
    public char letter1;
    public char letter2;
    public char letter3;
    private Client user;

    private int refresh = 0;

	void Start () {
        user = GameObject.Find("User").GetComponent<Client>();
        countDown = user.countDown;
        gameCountDown = user.gameCountDown;
        music = GetComponents<AudioSource>();
        prevLetter = '0';
        
    }
	
	void Update () {
        if (countDown >= 4)
        {
            countDownText.text = "LOADING...";
            countDown = user.countDown;
        }
        else if (countDown > 0)
        {
            countDownText.text = "COUNT DOWN:";
            if (countDown != 4 && countDown >= 1) textDisplay.text = "" + countDown;
            countDown = user.countDown;
            
        }
        else
        {
            if (user.gameCountDown <= 0) 
            {
                user.playing = false;
                music[0].Stop();
                controller.GetComponent<Controller>().enabled = false;
                endScreen.gameObject.SetActive(true);
                gameObject.SetActive(false);

            }
            countDownText.gameObject.SetActive(false);
            gameTime.text = "TIME: " + user.gameCountDown;
            if(!music[0].isPlaying) music[0].Play();
            if (letter1 != prevLetter)
            {
                music[1].Play();
            }
            prevLetter = letter1;
            letter1 = user.letter1;
            letter2 = user.letter2;
            letter3 = user.letter3;
            
            textDisplay.text = "" + letter1;
            if (letter2 != '0')
            {
                textDisplay.text += " + " + letter2;
                if (letter3 != '0')
                    textDisplay.text += " + " + letter3;
            }

            
            updateScores();
            
        }
	}

    private void updateScores()
    {
        score.text = "";
        int placing = 0;
        if (user.playerList != null)
        {

            for (int i = 0; i < user.playerList.Count; i++)
            {
                if (i > 0)
                {
                    if (user.playerList[i].score == user.playerList[i - 1].score)
                    {
                        placing--;
                    }
                }
                if (placing == 0)
                {
                    score.text += "1st " + user.playerList[i].name + " " + user.playerList[i].score + "\n";
                    placing++;
                }
                else if (placing == 1)
                {
                    score.text += "2nd " + user.playerList[i].name + " " + user.playerList[i].score + "\n";
                    placing++;
                }
                else if (placing == 2)
                {
                    score.text += "3rd " + user.playerList[i].name + " " + user.playerList[i].score + "\n";
                    placing++;
                }

                else if (i == 3)
                {
                    score.text += "4th " + user.playerList[i].name + " " + user.playerList[i].score + "\n";
                    placing++;
                }
                else if (i == 4)
                {
                    score.text += "5th " + user.playerList[i].name + " " + user.playerList[i].score + "\n";
                    placing++;
                }
            }
        }
    }
}
