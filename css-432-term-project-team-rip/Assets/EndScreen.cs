﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class is used to display the end results of the game, as well as to let the user make decisions for playing again or quitting the game.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Diagnostics;

public class EndScreen : MonoBehaviour {
    public Button playAgain;
    public Button quit;
    public Client user;
    public GameObject scoreboard;
    public Text finishedText;
    private Stopwatch timer;

    int getScores = 0;

	// Use this for initialization
	void Start () {
        timer = new Stopwatch();
        playAgain.onClick.AddListener(playAgainClick);
        quit.onClick.AddListener(quitClick);
        timer.Start();
        user = GameObject.Find("User").GetComponent<Client>();
        
	}
	
    private void playAgainClick()
    {
        
        user.setReady();
    }

    private void quitClick() //When the user clicks quit, we need to disconnect from the server
    {
        SceneManager.LoadScene("BackToRooms");
        if (user.ready)
            user.cancelReady();
        user.exitRoom();

    }
	// Update is called once per frame
	void Update () {
        if (timer.Elapsed.Seconds >= 3)
        {
            finishedText.gameObject.SetActive(false);
            playAgain.gameObject.SetActive(true);
            quit.gameObject.SetActive(true);
            scoreboard.gameObject.SetActive(true);
            getScores++; //All players may need to send a final request for final scores;

        }
        if (user.ready && !user.playing)
        {
            if (user.checkIfReady())
            {
                user.playGame();
                
                SceneManager.LoadScene("Game"); //Player's game needs to wait until everyone clicks play again, then load game
            }
        }
        if (getScores == 1)
        {
            setFinalScores();
            getScores++;
        }
		
	}

    private void setFinalScores()
    {
        user.getFinalScores();
        user.sortPlayers();
        Text score = scoreboard.GetComponentsInChildren<Text>()[1];
        score.text = "";
        int placing = 0;
        for (int i = 0; i < user.playerList.Count; i++)
        {
            if (i > 0)
            {
                if (user.playerList[i].score == user.playerList[i - 1].score)
                {
                    placing--;
                }
            }
            if (placing == 0)
            {
                score.text += "1st " + user.playerList[i].name + " " + user.playerList[i].score + "\n";
                placing++;
            }
            else if (placing == 1)
            {
                score.text += "2nd " + user.playerList[i].name + " " + user.playerList[i].score + "\n";
                placing++;
            }
            else if (placing == 2)
            {
                score.text += "3rd " + user.playerList[i].name + " " + user.playerList[i].score + "\n";
                placing++;
            }

            else if (i == 3)
            {
                score.text += "4th " + user.playerList[i].name + " " +  user.playerList[i].score + "\n";
                placing++;
            }
            else if (i == 4)
            {
                score.text += "5th " + user.playerList[i].name + " " + user.playerList[i].score + "\n";
                placing++;
            }
        }
    }

    
}
