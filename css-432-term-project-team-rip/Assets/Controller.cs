﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: The controller class is what handles input and delivers points to the server
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour {

    

    public GameView view;
    private AudioSource[] sounds;
    public Client user;

    string input = ""; // input buffer
    bool key1; //bool variables to check if the letters were pressed.
    bool key2;
    bool key3;
    int inputFlushCount = 0;

    
	// Use this for initialization
	void Start () {
        sounds = GetComponents<AudioSource>();
        key1 = false;
        key2 = false;
        key3 = false;
        user = GameObject.Find("User").GetComponent<Client>();
	}
	
	// Update is called once per frame
	void Update () {
        if (view.countDown <= 0)
        {

            if (inputFlushCount >= 10 || input.Length >= 15)
            {
                input = "";
                inputFlushCount = 0;
            }
            inputFlushCount++;
            if (Input.anyKeyDown)
            {
                key1 = false;
                key2 = false;
                key3 = false;
                
                input += Input.inputString.ToUpper(); 
                if (input.Length > 0) {
                    if (view.letter1 != '0' && view.letter2 == '0' && view.letter3 == '0') //single char
                    {
                        if (input[0] == view.letter1) //If correct letter, send to server a score add
                        {
                            user.score += 1; //send plus 1 to the server. Client will have to send score with ID for the server to identify (we will have to replace player1Score with something else)
                            sounds[0].Play();
                        }
                        else
                        {
                            user.score -= 1; //send minus 1 to the server
                            sounds[1].Play();
                        }
                        

                    }
                    else if (view.letter1 != '0' && view.letter2 != '0' && view.letter3 == '0') //Double letters
                    {
                        if (input[0] == view.letter1 || input[0] == view.letter2) //If we have to press the buttons on the exact same frame, what we need is an input buffer
                        {
                            bool badKey = false;
                            for (int i = 0; i < input.Length; i++)
                            {
                                if (input[i] == view.letter1) key1 = true;
                                else if (input[i] == view.letter2) key2 = true;
                                else
                                {
                                    badKey = true;
                                }
                            }
                            if (!badKey)
                            {
                                if (key1 && key2)
                                {
                                    user.score += 3; //Send scores to server
                                    sounds[0].Play();
                                }

                            }
                            else
                            {
                                user.score -= 1;
                                sounds[1].Play();
                            }


                        }
                        else
                        {
                            user.score -= 1;
                            sounds[1].Play();
                        }
                    }

                    else if (view.letter1 != '0' && view.letter2 != '0' && view.letter3 != '0') // Triple letters
                    {
                        if (input[0] == view.letter1 || input[0] == view.letter2 || input[0] == view.letter3) //This or statement (along with the 2nd if statement) is to make it so that you don't lose points if you just press one of the right keys
                        {
                            bool badKey = false;
                            for (int i = 0; i < input.Length; i++)
                            {
                                if (input[i] == view.letter1) key1 = true;
                                else if (input[i] == view.letter2) key2 = true;
                                else if (input[i] == view.letter3) key3 = true;
                                else
                                {
                                    badKey = true;
                                }
                            }
                            if (!badKey)
                            {
                                if (key1 && key2 && key3)
                                {
                                    user.score += 5; //Send scores to server
                                    sounds[0].Play();
                                }

                            }
                            else
                            {
                                user.score -= 1;
                                sounds[1].Play();
                            }


                        }
                        else
                        {
                            user.score -= 1;
                            sounds[1].Play();
                        }
                    }
                }
                

            }
        }
		
	}
}
