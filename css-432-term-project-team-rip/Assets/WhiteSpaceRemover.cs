﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class is used to remove white space in the first index of a string for a name. It is also used to remove ';' in the beginning of a string
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WhiteSpaceRemover : MonoBehaviour {

    

	void Start () {
		
	}
	

	void Update () {
        InputField input = gameObject.GetComponent<InputField>();
        if (input.text.Length >= 1 && (input.text[0] == ' ' || input.text[0] == ';'))
            input.text = "";
        
	}
}
