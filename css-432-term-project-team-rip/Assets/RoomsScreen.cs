﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class is what updates all of the room data in the lobby screen. It also updates the chat board as well.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomsScreen : MonoBehaviour {
    public Room[] rooms;

    private static GameObject thisObject;
    int refresh = 0;
    public Client client;
    public Text chatBoard;
    public Button send;
    public InputField currentText;
    public Text numOfUsers;
    public ScrollRect scrollView;
    public Button back;
    
    // Use this for initialization
    void Start () {
        rooms = GetComponentsInChildren<Room>();
        send.onClick.AddListener(sendMessage);
        client = GameObject.Find("User").GetComponent<Client>();
        back.onClick.AddListener(exit);
        
	}

    private void exit()
    {
        client.Disconnect();
    }

    private void sendMessage()
    {
        if (currentText.text != "")
        {
            char[] temp = currentText.text.ToCharArray();
            string message = "";
            
            for (int i = 0; i < currentText.text.Length; i++)
            {
                if (temp[i] != ';')
                    message += temp[i];
            }

            
            client.sendChatMessage(message);
            currentText.text = "";
            
        }
        
        currentText.ActivateInputField();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
            sendMessage();

        if (client.login && client.playing == false)
        {
            if (refresh >= 5)
            {
                string[] roomData = client.getAllRoomData();
                if (roomData != null)
                {
                    int j = 0;
                    for (int i = 0; i < rooms.Length; i++, j += 2) 
                    {
                        
                        rooms[i].count.text = roomData[j] + "/5";
  
                        if (Int32.Parse(roomData[j]) >= 5 || roomData[j + 1] == "True")
                        {
                            rooms[i].status.text = "STATUS: CLOSED";
                        }
                        else if (roomData[j + 1] == "False") rooms[i].status.text = "STATUS: OPEN"; //Game start = false, so room is open


                        
                    }
                    numOfUsers.text = "Number of users online: " + roomData[j];

                    

                    
                }

                string messages = client.getChat();
                if (messages != "")
                {
                    if (chatBoard.text.Length != messages.Length)
                    {
                        chatBoard.text = messages;
                        Canvas.ForceUpdateCanvases();
                        scrollView.verticalNormalizedPosition = 0f;
                    }
                    else
                        chatBoard.text = messages;
                    
                    
                }

                refresh = 0;
            }
            refresh++;

        }
    }

    
}
