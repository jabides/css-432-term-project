﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class is used on the exit button on the start screen to exit the application.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Exit : MonoBehaviour {


	// Use this for initialization
	void Start () {
        var button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(exit);
	}

    void exit()
    {
        Application.Quit();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
