﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: The Room class is used to update room status and to prevent players from entering a room.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Room : MonoBehaviour {

    public GameObject thisScreen;
    public GameObject nextScreen;
    private Client user;
    public Text playerListText;
    public Text roomNumber;
    public Text status;
    public Text count;
    public Button exitRoomButton;
    bool roomEntered;
    private int playerCount = 0;
    private string[] playerList;
    

	// Use this for initialization
	void Start () {
        var button = GetComponent<Button>();
        button.onClick.AddListener(OnClick);
        count.text = "" + playerCount;
        roomEntered = false;
        user = GameObject.Find("User").GetComponent<Client>();
	}

    private void OnClick()
    {
        if (playerCount < 5 && status.text == "STATUS: OPEN") // If the playerCount in the room is less than 5 and the room is open, the player may click on the room to enter
        {
            if (user.enterRoom(int.Parse(roomNumber.text))) //Tell server that user is entering roomNumber
            {
                
                nextScreen.SetActive(true);
                nextScreen.GetComponentsInChildren<Text>()[0].text = "ROOM " + roomNumber.text;
                user.room = int.Parse(roomNumber.text);
                thisScreen.SetActive(false);
            }
            

            
        }
    }

	void Update () {
        
		
	}
}
