﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class is used to display all players in a room. It gets a list of players every 5 frames and displays them. It also checks if all players are ready.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DisplayPlayers : MonoBehaviour {

    private Text list;
    public Text roomMessage;
    private Client user;
    int refresh = 0;
    public Button back;
    public Button ready;
    public Button cancel;

	void Start () {
        list = gameObject.GetComponentsInChildren<Text>()[1];
        back.onClick.AddListener(OnClick);
        ready.onClick.AddListener(play);
        cancel.onClick.AddListener(cancelGame);
        user = GameObject.Find("User").GetComponent<Client>();
        
	}

    private void play()
    {
        roomMessage.gameObject.SetActive(true);
        user.setReady();
        
    }

    private void cancelGame()
    {
        roomMessage.gameObject.SetActive(false);
        user.cancelReady();
        
    }
    private void OnClick() 
    {
        roomMessage.gameObject.SetActive(false);
        if (user.ready)
            user.cancelReady();
        user.exitRoom();
    }
 

    void Update () {

        if (refresh >= 5)
        {
            
            refresh = -1;
            string[] playerlist = user.getPlayerList();
            if (playerlist != null)
            {
                list.text = "";
                for (int i = 0; i < playerlist.Length; i++)
                {
                    if (playerlist[i] != "")
                    {
                        if (playerlist[i] == user.name) list.text += playerlist[i] + " (You)\n";
                        else list.text += playerlist[i] + "\n";
                    }
                }
            }

            if (user.ready && !user.playing) //If this user is ready, check if all users are ready to play
            {
                if (user.checkIfReady())
                {
                    user.playGame();
                    SceneManager.LoadScene("Game");
                }
            }
        }

        refresh++;


        


		
	}
}
