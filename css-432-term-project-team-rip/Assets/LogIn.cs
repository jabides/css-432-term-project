﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class manages the login feature. On clicking Start in the login screen, this class first checks if the IP address inputted is valid. It then checks if the
 *                      name is a valid name, returning error messages if the name is empty or contains a ';'. This class is also what calls startConnection() from the Client class, which
 *                      starts the connection from this client to the server. Once the connection has started, this class calls the checkName() method from the client class and uses the
 *                      name the user inputted. If all goes well, the user stays connected and moves onto the lobby.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogIn : MonoBehaviour{

    public Text name;
    public Text ipAddress;
    public Text ErrorMessage;
    public Canvas thisScreen;
    public Canvas nextScreen;
    public Client user;
    private bool load = false;

	// Use this for initialization
	void Start () {
        var button = GetComponent<Button>();
        button.onClick.AddListener(OnClick);
        user = GameObject.Find("User").GetComponent<Client>();

	}
	
    private void OnClick()
    {    
           
        ErrorMessage.text = "";
        
        if (!user.setIP(ipAddress.text))
        {
            ErrorMessage.text = "INVALID IP ADDRESS";
            load = false;
            return;
        }
        int check = checkValidity();
        if (check == -3)
        {
            ErrorMessage.text = "COULD NOT CONNECT TO SERVER";

        }
        else if (check == -5)
            ErrorMessage.text = "NAME CANNOT HAVE ';'";

        else if (check == -1)
            ErrorMessage.text = "CANNOT HAVE EMPTY NAME";
        else if (check == -2)
            ErrorMessage.text = "NAME ALREADY EXISTS";
        else if (check == -4)
            ErrorMessage.text = "SERVER TIMED OUT. TRY AGAIN.";
        else if (check == 0)
        {
            //Update user ID;
            nextScreen.gameObject.SetActive(true);
            thisScreen.gameObject.SetActive(false);
        }


    }
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Return))
            OnClick();
        
    }


    private int checkValidity()
    {
        if (name.text.Length == 0)
            return -1;

        for (int i = 0; i < name.text.Length; i++)
        {
            if (name.text[i] == ';')
                return -5;
        }


        if (!user.startConnection())
            return -3;

        int result = user.checkName(name.text);
        if (result == 0)
            return 0;  
        else if (result == -2)
            return -2;
        else
            return -4;

    }
}
