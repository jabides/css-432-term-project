﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This ChatRoom class is used to manage the chat data for the game. It contains a string list for containing every message, and methods for sending the chat data to the client
 *                      and adding chat messages from clients.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;


class ChatRoom
{

    private List<string> chatData;

    public ChatRoom()
    {
        chatData = new List<string>();
    }

    public void addText(string text)
    {
        text += "\n";
        if (chatData.Count > 50)
            chatData.RemoveAt(0);
        chatData.Add(text);
    }


    public void sendChat(Socket clientSocket)
    {
        string sendMessage = "Chat ";

        for (int i = 0; i < chatData.Count; i++)
        {
            sendMessage += chatData[i];
        }
        sendMessage += ";";

        clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
    }
}

