﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class is what runs the core of the server an all network functions. It first starts off asking if the user would like to input their own IP address. If not
 *                      the class will attempt to grab an IP address automatically using the getIPAddress() method. The server will listen at port 8000 for any clients, then start a thread
 *                      that runs the getClient() method for every client. It also starts 10 threads for the 10 rooms. The getClient() method receives all of the requests for a client. 
 *                      Server acts upon each request and sends responses to the client to confirm certain situations.
 *                      
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;

public class Server {
    private static Socket gameSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    private static byte[] dataBuffer = new byte[1024];
    volatile static public ServerData data;
    private static volatile ChatRoom chat = new ChatRoom();

    static private Object thisLock = new object();

    static void Main(string[] args)
    {
        string address = "";
        string input = "";
        while (true)
        {
            Console.Write("Manually input IP address? (y/n) ");
            input = Console.ReadLine();
            if (input.ToLower() == "y")
            {
                Console.WriteLine("Input IP address: ");
                address = Console.ReadLine();
                break;
            }
            else if (input.ToLower() == "n")
            {
                Console.WriteLine("Getting IP address...");
                address = getIPAddress();
                break;
            }
        }
        

        int port = 8000;
        IPEndPoint ipEnd = null;
        try
        {
            ipEnd = new IPEndPoint(IPAddress.Parse(address), port);
        }
        catch
        {

        }
        if (ipEnd == null)
        {
            Console.WriteLine("Could not parse IP address");
            return;
        }
        gameSocket.Bind(ipEnd);
        Console.WriteLine("Server is setup and the IP Address is: " + ipEnd.Address);

        
        data = new ServerData();
        Thread.Sleep(1000); //For some reason, this doesn't work if I loop it...
        Thread room0 = new Thread(new ThreadStart(() => data.roomList[0].Start()));
        room0.Start();
        Thread room1 = new Thread(new ThreadStart(() => data.roomList[1].Start()));
        room1.Start();
        Thread room2 = new Thread(new ThreadStart(() => data.roomList[2].Start()));
        room2.Start();
        Thread room3 = new Thread(new ThreadStart(() => data.roomList[3].Start()));
        room3.Start();
        Thread room4 = new Thread(new ThreadStart(() => data.roomList[4].Start()));
        room4.Start();
        Thread room5 = new Thread(new ThreadStart(() => data.roomList[5].Start()));
        room5.Start();
        Thread room6 = new Thread(new ThreadStart(() => data.roomList[6].Start()));
        room6.Start();
        Thread room7 = new Thread(new ThreadStart(() => data.roomList[7].Start()));
        room7.Start();
        Thread room8 = new Thread(new ThreadStart(() => data.roomList[8].Start()));
        room8.Start();
        Thread room9 = new Thread(new ThreadStart(() => data.roomList[9].Start()));
        room9.Start();

        Socket clientSocket;
        while (true) {
            gameSocket.Listen(100);
            clientSocket = gameSocket.Accept();
            Thread ListenClient = new Thread(new ThreadStart(() => Server.getClient(clientSocket)));
            ListenClient.Start();
            Thread.Sleep(1000);
        }
    }

    private static void getClient(Socket clientSocket)
    {
        Client thisClient = null;
        try
        {
            while (true)
            {
                int dataRecieved = clientSocket.Receive(dataBuffer, 0, dataBuffer.Length, SocketFlags.None);
                string output = Encoding.ASCII.GetString(dataBuffer, 0, dataRecieved);
                string[] requests = output.Split(';');

                if (requests != null)
                {
                    for (int r = 0; r < requests.Length; r++)
                    {
                        //Console.WriteLine("Request: " + requests[r]);

                        if (requests[r].Length == 0)
                        {
                            
                            
                        }
                        else if (requests[r].Equals("Exit"))
                        {
                            if (thisClient != null)
                            {
                                Console.WriteLine(thisClient.name + " logging out");
                                chat.addText("*" + thisClient.name + " logged out");
                                if (thisClient.roomEntered != -1)
                                {
                                    data.roomList[thisClient.roomEntered].enteredClients.Remove(thisClient);
                                }
                                data.clientList.Remove(thisClient);

                                
                            }

                            string sendexit = "Bye;";
                            sendToClient(clientSocket, sendexit);
                            clientSocket.Close();
                            return;
                        }

                        else if (requests[r].Length >= 10 && requests[r].Substring(0, 10) == "Check Name")
                        {
                            if (data.clientList.Count == 50) continue;
                            string exists = "";
                            string receiveName = requests[r].Substring(11);

                            if (data.clientList.Count == 0)
                            {
                                Console.WriteLine(receiveName + " logged in");
                                chat.addText("*" + receiveName + " logged in");
                                exists = "Doesn't exist;";
                                sendToClient(clientSocket, exists);
                                thisClient = new Client(receiveName);
                                data.clientList.Add(thisClient);
                            }

                            else
                            {
                                for (int i = 0; i < data.clientList.Count; i++)
                                {
                                    if (receiveName.Equals(data.clientList[i].name))
                                    {
                                        exists = "Exists;";
                                        sendToClient(clientSocket, exists);
                                        break;
                                    }
                                }

                                if (exists.Length == 0)
                                {
                                    Console.WriteLine(receiveName + " logged in");
                                    chat.addText("*" + receiveName + " logged in");
                                    exists = "Doesn't exist;";
                                    sendToClient(clientSocket, exists);

                                    thisClient = new Client(receiveName);
                                    data.clientList.Add(thisClient);
                                }
                            }

                            Console.WriteLine("List of clients: ");
                            for (int i = 0; i < data.clientList.Count; i++)
                            {
                                Console.WriteLine(data.clientList[i].name);
                            }
                            Console.WriteLine("Client count: " + data.clientList.Count);
                        }

                        else if (requests[r] == "Get All Room Data")
                        {

                            string sendMessage = "AllRoomData";
                            for (int i = 0; i < data.roomList.Count; i++)
                            {

                                sendMessage += " " + data.roomList[i].enteredClients.Count;
                                sendMessage += " " + data.roomList[i].gameStart;
                            }
                            
                            sendMessage += " " + data.clientList.Count + ";"; //Also get a count for number of users online

                            sendToClient(clientSocket, sendMessage);
                        }

                        else if (requests[r].Length >= 11 && requests[r].Substring(0, 11) == "Enter Room ")
                        {

                            string sendMessage = "";
                            string roomData = requests[r].Substring(11);
                            int room = int.Parse(roomData) - 1;
                            //Console.WriteLine("Enter Room " + room);

                            if (data.roomList[room].enteredClients.Count >= 5 ||
                                data.roomList[room].gameStart)
                            {
                                sendMessage = "Cannot Enter Room;";
                            }
                            else
                            {
                                sendMessage = "Entered Room;";
                                if (thisClient.roomEntered == -1)
                                {
                                    bool ok = true;
                                    for (int i = 0; i < data.roomList[room].enteredClients.Count; i++)
                                    {
                                        if (data.roomList[room].enteredClients[i].name == thisClient.name)
                                        {
                                            ok = false;
                                            break;
                                        }
                                    }
                                    if (ok)
                                    {
                                        data.roomList[room].enteredClients.Add(thisClient);
                                    }
                                    thisClient.roomEntered = room;
                                }
                            }
                            sendToClient(clientSocket, sendMessage);
                        }

                        else if (requests[r] == "Get Player List")
                        {

                            string sendMessage = "Players ";
                            if (thisClient.roomEntered != -1)
                            {
                                for (int i = 0; i < data.roomList[thisClient.roomEntered].enteredClients.Count; i++)
                                {
                                    sendMessage += data.roomList[thisClient.roomEntered].enteredClients[i].name + "|";
                                }
                            }
                            sendMessage += ";";

                            sendToClient(clientSocket, sendMessage);
                        }

                        else if (requests[r] == "Exit Room")
                        {
                            if (thisClient.roomEntered != -1)
                            {
                                data.roomList[thisClient.roomEntered].enteredClients.Remove(thisClient);
                            }
                            thisClient.roomEntered = -1;

                            thisClient.ready = false;


                            string sendMessage = "Exited;";
                            sendToClient(clientSocket, sendMessage);


                        }

                        else if (requests[r] == "Ready")
                        {

                            thisClient.ready = true;
                            Thread readyThread = new Thread(new ThreadStart(() => data.roomList[thisClient.roomEntered].checkAllReady(clientSocket, thisClient)));
                            readyThread.Start();
                            string sendMessage = "Set To Ready;";
                            sendToClient(clientSocket, sendMessage);
                        }

                        else if (requests[r] == "Cancel")
                        {
                            thisClient.ready = false;
                            string sendMessage = "Cancelled;";
                            sendToClient(clientSocket, sendMessage);
                        }
                        else if (requests[r] == "Play")
                        {
                            if (data.roomList[thisClient.roomEntered].allSent > 0 && data.roomList[thisClient.roomEntered].gameStart)
                            {

                                while (data.roomList[thisClient.roomEntered].allSent < data.roomList[thisClient.roomEntered].enteredClients.Count) {
                                    Thread.Sleep(500);
                                }
                                Console.WriteLine(thisClient.name + " is now playing a game");
                                string sendMessage = "Now Playing;";
                                
                                sendToClient(clientSocket, sendMessage);

                                clientSocket.ReceiveTimeout = 100;
                                playGame(clientSocket, thisClient);


                                requests = null;
                                
                                clientSocket.ReceiveTimeout = 0; //Go back to infinite receive time
                                break;
                            }
                        }

                        else if (requests[r] == "Get Final Scores")
                        {
                            string sendMessage = "Final ";
                            if (thisClient.roomEntered != -1)
                            {
                                for (int i = 0; i < data.roomList[thisClient.roomEntered].enteredClients.Count; i++)
                                {
                                    sendMessage += data.roomList[thisClient.roomEntered].enteredClients[i].name + "|";
                                    sendMessage += data.roomList[thisClient.roomEntered].enteredClients[i].score + "|";
                                }
                            }

                            sendMessage += ";";

                            Console.WriteLine("Sent: " + sendMessage);
                            sendToClient(clientSocket, sendMessage);
                        }
                        else if (requests[r].Length > 4 && requests[r].Substring(0, 4) == "Chat")
                        {
                            string receiveMessage = thisClient.name + ": " + requests[r].Substring(5);
                            chat.addText(receiveMessage);
                        }

                        else if (requests[r] == "Get Chat")
                        {
                            chat.sendChat(clientSocket);
                        }
                    }
                }

            }

        }
        catch (SocketException e) //If socket crashes, remove client;
        {
            if (thisClient != null)
            {
                Console.WriteLine("Client Connection Crashed... Removing Client: " + thisClient.name);
                if (thisClient.roomEntered != -1)
                {
                    chat.addText("*" + thisClient.name + " logged out");
                    data.roomList[thisClient.roomEntered].enteredClients.Remove(thisClient);
                }

                
                data.clientList.Remove(thisClient);
            }
        }

        clientSocket.Close();
    }

    private static string getIPAddress()
    {
        IPAddress[] ips = Dns.GetHostAddresses(Dns.GetHostName());
        foreach (IPAddress i in ips)
        {
            if (i.AddressFamily == AddressFamily.InterNetwork)
            {
                return i.ToString();
            }
        }
        
        System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();

        return "127.0.0.1";
    }


    private static void playGame(Socket clientSocket, Client thisClient)
    {
        
        data.roomList[thisClient.roomEntered].gameCountDown = 60; //Game doesn't run until count down is 0. Therefore countdown will stay up until the last client gets through here, thus synchronizing clients.
        data.roomList[thisClient.roomEntered].countDown = 5;
        while (data.roomList[thisClient.roomEntered].gameCountDown > -2)
        {
            Thread.Sleep(5); //So that it doesn't use up too much of the CPU time, make it sleep
            //data.roomList[thisClient.roomEntered].sortPlayers();
            
            //Send game data
            string sendMessage = "GAME ";

            for (int i = 0; i < data.roomList[thisClient.roomEntered].enteredClients.Count; i++)
            {
                sendMessage += data.roomList[thisClient.roomEntered].enteredClients[i].name + " ";
                sendMessage += data.roomList[thisClient.roomEntered].enteredClients[i].score + " ";
            }
            sendMessage += "| ";

            sendMessage += data.roomList[thisClient.roomEntered].countDown + " ";
            sendMessage += data.roomList[thisClient.roomEntered].gameCountDown + " ";
            sendMessage += data.roomList[thisClient.roomEntered].letter1 + " ";
            sendMessage += data.roomList[thisClient.roomEntered].letter2 + " ";
            sendMessage += data.roomList[thisClient.roomEntered].letter3 + " ";
            sendMessage += ";";
            //Add sequence number?
            //Console.WriteLine("Sent: " + sendMessage);
            sendToClient(clientSocket, sendMessage);


            //Receive player points
            int dataReceived = 0;
            string messageReceived = "";
            try
            {
                dataReceived = clientSocket.Receive(dataBuffer, 0, dataBuffer.Length, SocketFlags.None);
                messageReceived = Encoding.ASCII.GetString(dataBuffer, 0, dataReceived);
            }
            catch (SocketException e) //In case of timeout
            {

            }

            if (messageReceived != "")
            {
                //Console.WriteLine("Received: " + messageReceived);
                string[] contents = messageReceived.Split(';');

                for (int i = 0; i < contents.Length; i++)
                {
                    if (contents[i].Length >= 5 && contents[i].Substring(0, 5) == "Point")
                    {
                        
                        try
                        {
                            thisClient.score = Int32.Parse(contents[i].Substring(6)); //Add point (format: "Point <score add>")
                        }
                        catch (Exception e)
                        {

                            continue;
                        }
                    }
                    else if (contents[i] == "Exit Room")
                    {
                        if (thisClient.roomEntered != -1)
                        {
                            data.roomList[thisClient.roomEntered].enteredClients.Remove(thisClient);
                        }
                        thisClient.roomEntered = -1;

                        thisClient.ready = false;


                        sendMessage = "Exited;";
                        sendToClient(clientSocket, sendMessage);
                        return;

                    }
                    else if (contents[i] == "Exit")
                    {
                        data.roomList[thisClient.roomEntered].gameStart = false;
                        if (thisClient.roomEntered != -1)
                        {
                            data.roomList[thisClient.roomEntered].enteredClients.Remove(thisClient);
                            thisClient.roomEntered = -1;
                            chat.addText("*" + thisClient.name + " logged out");
                        }
                        data.clientList.Remove(thisClient);

                        string sendexit = "Bye;";
                        sendToClient(clientSocket, sendexit); 
                        Thread.Sleep(500);
                        clientSocket.Close();
                        return;

                    }
                }

            }
        }

        thisClient.ready = false;


    }

    static public void sendToClient(Socket clientSocket, string sendMessage)
    {
        lock (thisLock)
        {
            clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendMessage), 0, sendMessage.Length, SocketFlags.None);
        }
    }

}
