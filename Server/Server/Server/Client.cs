﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class is used to contain information about a client. This includes a the client's name, their current score, the room they entered, and if they are
 *                      ready to play.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;

public class Client
{

    volatile public string name;
    volatile public int score;
    volatile public int roomEntered;
    volatile public bool ready;

    public Client(string n) 
    {
        name = n;
        ready = false;
        score = 0;
        roomEntered = -1;
    }


}
