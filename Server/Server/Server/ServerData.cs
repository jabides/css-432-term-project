﻿/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class is used to maintain a list of all clients and a list of all rooms.
 */

using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class ServerData {
    volatile public List<Client> clientList = new List<Client>();
    volatile public List<ServerRoom> roomList = new List<ServerRoom>();

    public ServerData()
    {
        Thread initialize = new Thread(new ThreadStart(() => init()));
        initialize.Start();
    }

    private void init()
    {
        for (int i = 0; i < 10; i++)
        {
            roomList.Add(new ServerRoom(i));
        }
    }
}
