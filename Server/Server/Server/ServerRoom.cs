﻿
/* 
 *  Authors:        JJ Abides and Vikas Sidhu
 *  Date:           March 7, 2018
 *  Course:         CSS 432
 *  Professor:      Brent Lagesse
 *  Assignment:     Term Project
 *  
 *  Class Description: This class is the core of the game logic. It is meant to be ran as a thread from the server. It maintains all variables necessary for the game:
 *                      count down time, game time, list of clients in room, letters to press, and room game status. The thread starts off by running in the Start() method.
 *                      It goes into the runGame() method once all clients have been set to ready and have been notified to play. Once the game starts, it goes into a 3 second
 *                      count down. Afterward, the game timer counts down from 60. A letter is generated randomly between 1-5 seconds. The chance of a random letter change between these
 *                      1-5 seconds is 1/300 in every loop. The chance that there will be only 1 letter press is 6/12. The chance for 2 letters is 4/12. For 3 letters, it's 2/12.
 *                      The class comes with a random letter generation method, which ensures that the current letter is not the same as the last 2 letters that were displayed.
 */

using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class ServerRoom  {
    volatile public int roomNumber;

    //user list 
    public volatile int countDown = 4; //Starting count down
    public volatile int gameCountDown = 60; //Game timer
    private Random rand;
    public float letterHangTime;
    public char letter;
    public volatile char letter1 = '0';
    public volatile char letter2 = '0';
    public volatile char letter3 = '0';
    public volatile int allSent = 0;
    private Stopwatch timer;
    private Stopwatch gameTimer;
    volatile public bool gameStart; //This will be used to start the game. This becomes true once all clients are confirmed ready to play and have loaded to the game screen

    volatile public List<Client> enteredClients;

    private int previous1 = -1; //These 2 variables are to ensure that the current chosen letter is not the same as the last 2
    private int previous2 = -1;

    public ServerRoom(int room)
    {
        roomNumber = room;
        gameStart = false;
        enteredClients = new List<Client>();
    }

    
    public void checkAllReady(Socket clientSocket, Client thisClient)
    {


        while (enteredClients.Count > 0 && thisClient.ready)
        {
            bool allGood = true;
            for (int i = 0; i < enteredClients.Count; i++)
            {
                if (enteredClients[i].ready == false) //If current client in index is not ready, not everyone is ready to play
                {
                    allGood = false;
                    break;
                }
            }

            if (allGood)
            {
                string sendMessage = "All good;";
                gameStart = true;
                allSent++;
                Console.WriteLine("Sent All good");
                Server.sendToClient(clientSocket, sendMessage);
                break;
            }
            Thread.Sleep(100);
            
        }

    }


	// Use this for initialization
	public void Start () {
        //if (gameTimer == true)
        
        while(true)
        {
            Thread.Sleep(100);
            if (enteredClients.Count == 0)
            {
                gameStart = false;
                //break; 
            }
            
            if (gameStart && allSent >= enteredClients.Count)
            {
                runGame();
            }
        }
	}

	
	void runGame () { //run game logic
        timer = new Stopwatch();
        rand = new Random((int) DateTime.Now.Ticks & 0x000FFFF);
        gameTimer = new Stopwatch();
        countDown = 5;
        gameCountDown = 60;
        letterHangTime = 5;
        timer.Start();
        //gameStart = true;
        letter1 = '0';
        letter2 = '0';
        letter3 = '0';
        Console.WriteLine("Game Starting in room " + (roomNumber + 1));
        for (int i = 0; i < enteredClients.Count; i++)
        {
            enteredClients[i].score = 0;
        }

        Thread.Sleep(2000);//Let all players catch up to game
        while (true)
        {
            Thread.Sleep(10);
            if (enteredClients.Count == 0) break; //If all players just leave the room during a match
            
            if (countDown > 0)
            {
                if (timer.Elapsed.Seconds >= 1)
                {
                    countDown--;
                    timer.Reset();
                    timer.Start();
                }
            }
            else
            {
                if (gameCountDown == -1)
                {
                    gameCountDown--;
                    break;
                }
                if (!gameTimer.IsRunning) gameTimer.Start();
                else if (gameTimer.Elapsed.Seconds >= 1)
                {
                    gameCountDown--;
                    gameTimer.Reset();
                    gameTimer.Start();
                }
                //Start game
                if ((letterHangTime >= 1 && rand.Next(300) == 0) || letterHangTime >= 5)
                {
                    int type = rand.Next(0, 13);
                    if (type >= 0 && type < 7)
                    {
                        letter1 = randLetter();
                        letter2 = '0';
                        letter3 = '0';
                    }
                    else if (type >= 7 && type < 11)
                    {

                        letter1 = randLetter();
                        letter2 = randLetter();
                        letter3 = '0';
                    }
                    else
                    {
                        letter1 = randLetter();
                        letter2 = randLetter();
                        letter3 = randLetter();
                    }

                    //This is where we send the letter to the clients

                    letterHangTime = 0;
                    timer.Reset();
                    timer.Start();
                }

                else
                {
                    letterHangTime = timer.Elapsed.Seconds;
                }
            }
        }

        allSent = 0;
        
	}

    private char randLetter()
    {
        int select = rand.Next(0, 26);

        while (select == previous1 || select == previous2)
        {
            select = rand.Next(0, 26);   
        }
        previous2 = previous1;
        previous1 = select;

        switch (select)
        {
            case 0: letter = 'Q';
                break;
            case 1: letter = 'W';
                break;
            case 2: letter = 'E';
                break;
            case 3: letter = 'R';
                break;
            case 4: letter = 'T';
                break;
            case 5: letter = 'Y';
                break;
            case 6: letter = 'U';
                break;
            case 7: letter = 'I';
                break;
            case 8: letter = 'O';
                break;
            case 9: letter = 'M';
                break;
            case 10: letter = 'P';
                break;
            case 11: letter = 'A';
                break;
            case 12: letter = 'S';
                break;
            case 13: letter = 'D';
                break;
            case 14: letter = 'F';
                break;
            case 15: letter = 'G';
                break;
            case 16: letter = 'H';
                break;
            case 17: letter = 'J';
                break;
            case 18: letter = 'K';
                break;
            case 19: letter = 'L';
                break;
            case 20: letter = 'Z';
                break;
            case 21: letter = 'X';
                break;
            case 22: letter = 'C';
                break;
            case 23: letter = 'V';
                break;
            case 24: letter = 'B';
                break;
            case 25: letter = 'N';
                break;
            
        }
        return letter;
    }

    public void sortPlayers()
    {
        enteredClients.Sort(delegate (Client a, Client b)
        {
            if (a == null && b == null) return 0;
            else if (a == null) return -1;
            else if (b == null) return 1;
            else return b.score.CompareTo(a.score);
        });
    }
}
